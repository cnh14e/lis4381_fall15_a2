<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>CRSXXXX - Assignment2</title>		

<!-- Bootstrap core CSS -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Custom styles with this template -->
<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

  </head>

  <body>
  
  	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#" target="_self">Home</a>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="../index.php">CRSXXXX</a></li>
					<li><a href="a1/index.php">A1</a></li>
					<li><a href="a2/index.php">A2</a></li>
					<li><a href="a3/index.php">A3</a></li>
					<li><a href="a4/index.php">A4</a></li>
					<li><a href="a5/index.php">A5</a></li>
					<li><a href="p1/index.php">P1</a></li>
					<li><a href="p2/index.php">P2</a></li>
					<li><a href="../test/index.php">Test</a></li>					
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

<?php
date_default_timezone_set('America/New_York');
$today = date("m/d/y g:ia");
echo $today;
 ?>
	

  <?php include_once("../global/nav.php"); ?>
	
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start form-building -->
<!--
Standard rules for all Bootstrap form layouts:
Always use <form role="form"> (helps improve accessibility for people using screen readers)
Wrap labels and form controls in <div class="form-group"> (needed for optimum spacing)
Add class .form-control to all textual <input>, <textarea>, and <select> elements

<label> element does not render anything special for user.
However, it provides usability improvement for mouse users:
if user clicks on text within <label> element, it toggles control.

"for" attribute specifies which form element a label is bound to.
Should be equal to id attribute of related element to bind them together.

A label can be bound to an element either by using "for" attribute, or by placing element inside <label> element.
-->				<center><h1>Assignment 2</h1>
				<h2>Bootstrap Starter Template</h2>
				<p>Use this document as a way to quickly start any new project</br>
				All you get is this text and a mostly barebones HTML document<p></center>
				
				<form class="form-horizontal" role="form" method="post" action="process.php">

					<div class="form-group">
						<label class="control-label col-sm-2" for="fname">First Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="fname" id="fname" placeholder="Enter first name">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2" for="lname">Last Name:</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="lname" id="lname" placeholder="Enter last name">
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Email:</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-sm-2" for="pwd">Password:</label>
						<div class="col-sm-10">          
							<input type="password" class="form-control" name="pwd" id="pwd" placeholder="Enter password">
						</div>
					</div>
				
					<div class="form-group">
						<label class="control-label col-sm-2" for="comments">Comments:</label>
						<div class="col-sm-10">          
							<textarea class="form-control" rows="3" name="comments" id="comments" placeholder="Enter comments"></textarea>
						</div>
					</div>

					<div class="form-group">        
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-default">Submit</button>
						</div>
					</div>
				</form>
				<!-- End form-building -->
				
  <?php include_once "global/footer.php"; ?>

		</div> <!-- starter-template -->
    </div> <!-- end container -->

    <!-- Bootstrap core JavaScript: jQuery necessary for Bootstrap's JavaScript plugins
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
